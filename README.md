# CS3060_FA2018_A03_ZIMCOSKY

## Summary of code

### Task 1: Fibonacci Sequence

The first task was to Implement a function to calculate the Fibonacci series recursively using Guards and Pattern Matching. It seemed like pattern matching was necessary no matter how I chose to approach the algorithm. Technically, I could have used the classic method with double recursion:

```erlang
fib(0) -> 0;
fib(1) -> 1;
fib(N) -> fib(N-1) + fib(N-2).
```

But this approach makes the use of gaurds seem more than a bit gratuitous:

```erlang
fib(0) -> 0;
fib(1) -> 1;
fib(N) when n > 1 -> fib(N-1) + fib(N-2).
```

In order for guards to make sence, I chose an iterative approach, that also proved to be more efficient in performance. here is the fibinacci function I actually decided to use:

```erlang
fibonacci(N) ->
    fib(N,0,1).

fib(0, Result, _Next) ->
        Result;
fib(I, Result, Next) when I > 0 ->
    fib(I-1, Next, Result+Next).

```

### Task 2: Ping and Pong

The second task I had to create two processes, *ping* and *pong*. Ping sends a message to pong who sends a message back to ping. This process happens *N* times where a single “time” is considered as a message sent between both ping and pong.
I used function, *start/1* that takes a number, *N*.

```erlang
start(N) ->
    Pong_PID = spawn(a3, pong, []),
    spawn(a3, ping, [N, Pong_PID]).
```

This function first establishes a process identity for the "pong" process, then creates a process for "ping" with *N* as the first value, which is how many times these processes occur.

The first clause of *ping/2* sends the atom *finished* to "pong" and displays a message indicating that ping is finished handling messages:

```erlang
ping(0, Pong_PID) ->
    Pong_PID ! finished,
    io:format("ping finished~n", []);
```

The second clause of *ping/2* sends a message conaining the atom *ping* to "pong" along with a PID containg the process for "ping". Then, it waits to receive a message back from "pong", expecting the atom *pong*, and prints a confirmation message. Finally, a recursive call is made to *ping/2*, decrementing the *N* value:

```erlang
ping(N, Pong_PID) ->
    Pong_PID ! {ping, self()},
    receive
        pong ->
            io:format("Ping received pong~n", [])
    end,
    ping(N - 1, Pong_PID).
```

The *pong/0* function recieves a message from "ping". If the message contains the atom *finished*, "pong" displays a message indicating it is finished handling messages. If the message contains the atom *ping*, along with a PID containg the process for "ping", a confirmation message is printed, and sends a message containing the atom *pong* to "ping".

```erlang
pong() ->
    receive
        finished ->
            io:format("Pong finished~n", []);
        {ping, Ping_PID} ->
            io:format("Pong received ping~n", []),
            Ping_PID ! pong,
            pong()
    end.
```

### Output

```bash
1> c('a3').
a3.erl:5: Warning: export_all flag enabled - all functions will be exported
{ok,a3}
2> a3:fibonacci(0).
0
3> a3:fibonacci(1).
1
4> a3:fibonacci(2).
1
5> a3:fibonacci(3).
2
6> a3:fibonacci(4).
3
7> a3:fibonacci(5).
5
8> a3:fibonacci(6).
8
9> a3:fibonacci(7).
13
10> a3:fibonacci(8).
21
11> a3:fibonacci(9).
34
12> a3:fibonacci(10).
55
13> a3:fibonacci(64).
10610209857723
14> a3:start(0).
ping finished
Pong finished
<0.96.0>
15> a3:start(1).
Pong received ping
<0.99.0>
Ping received pong
ping finished
Pong finished
16> a3:start(2).
Pong received ping
<0.102.0>
Ping received pong
Pong received ping
Ping received pong
ping finished
Pong finished
17> a3:start(5).
Pong received ping
<0.105.0>
Ping received pong
Pong received ping
Ping received pong
Pong received ping
Ping received pong
Pong received ping
Ping received pong
Pong received ping
Ping received pong
ping finished
Pong finished
```