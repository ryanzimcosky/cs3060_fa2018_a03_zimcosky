%% Ryan Zimcosky
%% Assignment 3

- module(a3).
- compile(export_all).

%%   ╷--------╷
%%   | Task 1 |
%%   ╵--------╵
fibonacci(N) -> 
    fib(N,0,1).

fib(0, Result, _Next) ->
        Result;
fib(I, Result, Next) when I > 0 ->
    fib(I-1, Next, Result+Next).


%%   ╷--------╷
%%   | Task 2 |
%%   ╵--------╵

ping(0, Pong_PID) ->
    Pong_PID ! finished,
    io:format("ping finished~n", []);

ping(N, Pong_PID) ->
    Pong_PID ! {ping, self()},
    receive
        pong ->
            io:format("Ping received pong~n", [])
    end,
    ping(N - 1, Pong_PID).

pong() ->
    receive
        finished ->
            io:format("Pong finished~n", []);
        {ping, Ping_PID} ->
            io:format("Pong received ping~n", []),
            Ping_PID ! pong,
            pong()
    end.

start(N) ->
    Pong_PID = spawn(a3, pong, []),
    spawn(a3, ping, [N, Pong_PID]).